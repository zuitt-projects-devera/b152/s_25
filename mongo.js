//get average of all products on sale
/*db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "stocksAvg", total: {$avg: "$stocks"}}}
])*/

//get average of each suppliers
/*db.fruits.aggregate([
{$match: {onSale: true}},
{$group: {_id: "$supplier", avgStock: {$avg: "$stocks"}}}
])*/

//$max - highest number of stocks for all items on sale.
/*
db.fruits.aggregate ([
{$match: {onSale: true}},
{$group: {_id: "$supplier", maxStock: {$max: "$stocks"}}}
])*/

//$min - lowest number of stocks for all items on sale.
/*
db.fruits.aggregate ([
{$match: {onSale: true}},
{$group: {_id: "$supplier", minStock: {$min: "$stocks"}}}
])*/


/*db.fruits.aggregate ([
{$match: {onSale: true}},
{$group: {_id: "$supplier", lowestStock: {$min: "$stocks"}}}
])*/
//other stages
//$count - is a stage we can add after a match stage to count all items
//that matches our criteria
/*
db.fruits.aggregate([
    {$match: {price: {$lt: 50}}},
    {$count: "itemsLessThan50"}
])*/
/*db.fruits.aggregate([
    {$match: {onSale: false}},
    {$count: "itemsNotForSale"}
])
    db.fruits.aggregate([
    {$match: {stocks: {$lt: 20}}},
    {$count: "stocksLessThan20"}
])*/
//counts total number of items per group
/*db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier", noOfItems: {$sum: 1}}}
])*/
    //each item in the group will be assigned a value of 1 or counted as 1 and with the $sum operator
    //we can add each item in the group and arrive at the total number of items
  
  /*db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier", noOfItems: {$sum:1}, totalStock: {$sum: "$stocks"}}}
    ])*/
    //total number of items per supplier, total number of stock of items per supplier, list of items per supplier
    /*db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: {
            _id: "$supplier", 
            noOfItems: {$sum: 1}, 
            totalStocks: {$sum:"$stocks"},
            itemsPricesPerSupplier: {$addToSet: "$price"} //accumulates and adds the given fields into an array
        }
        }
        ])*/
//$out stage - will save/output your aggregated result from $group in a new collection.
//note: This will overwrite the collection if it already exists
/*db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier", totalStock: {$sum: "$stocks"}}},
    {$out: "totalStocksPerSupplier"}
])*/


    