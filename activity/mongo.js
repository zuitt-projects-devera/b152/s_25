//count the total number of items supplied by Red Farms
db.fruits.aggregate
	([
		{$match: {supplier: "Red Farms Inc."}},
		{$count: "RedFarmsTotalNumberofItems"}
	])

//count the total number of items with price greater than 50
db.fruits.aggregate
	([
		{$match: {price: {$gt: 50}}},
		{$count: "itemsGreaterThan50"}
	])

//get the avg price of all fruits that are onSale per supplier
db.fruits.aggregate
	([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier", avgPriceperSupplier: {$avg: "$price"}}}
	])

//get the highest price of fruits that are onSale per supplier
db.fruits.aggregate
	([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier", highestPricePerSupplier: {$max: "$price"}}}
	])

//get the lowest price of fruits that are onSale per supplier
db.fruits.aggregate
	([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier", lowestPricePerSupplier: {$min: "$price"}}}
	]) 